package org.catools.common.concurrent;

import lombok.extern.slf4j.Slf4j;
import org.catools.common.concurrent.exceptions.CStorageEmptyInitException;
import org.catools.common.utils.CDateUtil;
import org.catools.common.utils.CSleeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@Slf4j
public class CStorage<T> {
    private final Object lock = new Object();

    private final List<T> available = new ArrayList<>();
    private final List<T> borrowed = new ArrayList<>();

    private final Logger logger;
    private final int requestIntervalInSeconds;
    private final int requestTimeoutInSeconds;

    public CStorage(String name, int requestIntervalInSeconds, int requestTimeoutInSeconds) {
        this.logger = LoggerFactory.getLogger("Storage " + name);
        this.requestIntervalInSeconds = requestIntervalInSeconds;
        this.requestTimeoutInSeconds = requestTimeoutInSeconds;
    }

    public void init(List<T> initialObjects) {
        logger.info("Storage initiation...");
        if (initialObjects.isEmpty()) {
            throw new CStorageEmptyInitException("Attempt to initiate storage with empty list.");
        }

        performActionOnQueue(() -> {
            available.addAll(initialObjects);
            return true;
        });
        logger.info("Storage initiated.");
    }

    public <R> R performAction(String borrower, Function<T, R> action) {
        return performAction(borrower, t -> true, action);
    }

    public <R> R performAction(String borrower, Predicate<T> predicate, Function<T, R> action) {
        T t = null;
        try {
            t = borrow(borrower, predicate);
            return action.apply(t);
        } finally {
            if (t != null) {
                release(t);
            }
        }
    }

    public T borrow(String borrower) {
        return borrow(borrower, t -> true);
    }

    public T borrow(String borrower, Predicate<T> predicate) {
        performActionOnQueue(() -> {
            logger.trace("Attempt to borrow object for " + borrower);
            logger.trace("Storage contains {} available and {} borrowed objects", available.size(), borrowed.size());
            return true;
        });
        Request request = new Request(borrower, requestTimeoutInSeconds, predicate);
        return waitForObjectToBeAvailable(request);
    }

    public boolean release(T t) {
        if (t != null) {
            return performActionOnQueue(() -> {
                logger.trace("Object returned to storage. " + t.toString());
                borrowed.remove(t);
                available.add(t);
                logger.trace("Storage contains {} available and {} borrowed objects", available.size(), borrowed.size());
                return true;
            });
        }
        return false;
    }

    private T waitForObjectToBeAvailable(Request request) {
        do {
            T response = performActionOnQueue(() -> {
                T firstOrElse = available.isEmpty() ? null : (T) available.stream().filter(request.predicate).findFirst().orElse(null);
                if (firstOrElse != null) {
                    available.remove(firstOrElse);
                    borrowed.add(firstOrElse);
                    return firstOrElse;
                }
                if (request.isTimeOuted()) {
                    throw new RuntimeException("Request Timeout triggered for TestCase:" + request.borrower);
                }
                return null;
            });

            if (response != null) {
                return response;
            }

            CSleeper.sleepTightInSeconds(requestIntervalInSeconds);
        } while (true);
    }

    private synchronized <T> T performActionOnQueue(Supplier<T> supplier) {
        synchronized (lock) {
            return supplier.get();
        }
    }

    static class Request<T> {
        private final Date timeoutAt;
        private final String borrower;
        private final Predicate<T> predicate;

        public Request(String borrower, int timeoutInSeconds, Predicate<T> predicate) {
            this.timeoutAt = CDateUtil.add(new Date(), Calendar.SECOND, timeoutInSeconds);
            this.predicate = predicate;
            this.borrower = borrower;
        }

        public boolean isTimeOuted() {
            return timeoutAt.before(new Date());
        }
    }
}
