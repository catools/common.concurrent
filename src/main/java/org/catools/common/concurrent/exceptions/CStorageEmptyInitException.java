package org.catools.common.concurrent.exceptions;

public class CStorageEmptyInitException extends RuntimeException {

    public CStorageEmptyInitException(String message) {
        super(message);
    }
}
